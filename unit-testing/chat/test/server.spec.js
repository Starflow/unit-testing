const server = require('../server.js');
const config = require('../config/index.js');
const fs = require('fs-extra');
const assert = require('assert');
const { COPYFILE_EXCL } = fs.constants;
const http = require('http');
// console.log("http: ", fs);

describe('GET request', () => {
	let app;
	before((done) => {
		app = server.listen(3333, () => {
			done();
		});
	});
	

	// Написать тесты на
	// get запрос к http://localhost:3333  => вернет index.html
	// get запрос к http://localhost:3333/file.txt => вернет file.txt



	it('Index Get testing',  (done) => {
        let data = '';
        console.log('hello');
        return http.get('http://localhost:3333/', (resp) => {
			// A chunk of data has been recieved.
            console.log('tut norm');
            console.log(resp);
			resp.on('data', (chunk) => {
				data += chunk;
        	});
			resp.on('end',() => {
                console.log(data);
				assert.equal(data,fs.readFileSync(config.publicRoot+'/index.html'));
				done();
			})
		});
	});

    it('Text Get testing',  (done) => {
        let data = '';
        console.log('hello');
        return http.get('http://localhost:3333/text.txt', (resp) => {
            // A chunk of data has been recieved.
            console.log('tut norm');
            console.log(resp);
            resp.on('data', (chunk) => {
                data += chunk;
            });
            resp.on('end',() => {
                console.log(data);
                assert.equal(data,'Not found');
                done();
            })
        });


    });

	after((done) => {
		app.close( ()=> {
		done();
		});
	});

});
